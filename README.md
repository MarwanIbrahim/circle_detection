# Circle Detection 

This repo implements a few basic methods for circle detection 
(based on the coding trivia on 01.03.2023 and 15.02.2023). 

Implementations of
the different methods can be found under [implementations.py](implementations.py), while a 
comparison between the different methods can be found under [main.py](main.py)

The different methods considered for circle detection are:
1. Template matching (solution1)
2. Using FFT to detect circles in an image (solution2)
3. Lucas-Kanade tracker to find the circle radius given a possible circle point (solution3)
4. Simple implementation of circle Hough transform (solution4)
5. OpenCV implementation of circle Hough transform (solution4_cv)

Note that this repo is far from complete, and should rather be used to discuss how someone may implement 
circle detection using different methods 😃
