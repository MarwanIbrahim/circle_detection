import numpy as np
import cv2


def create_template(radius, filled=True, color=255, kernel=None):
    if kernel is None:
        kernel = np.zeros((2 * (radius + 1), 2 * (radius + 1)))
    size = kernel.shape
    cv2.circle(kernel, (size[1] // 2, size[0] // 2), radius, color, -1 if filled else 1)
    return kernel / np.sum(kernel)


def conv2d(I, f):
    # When computing fft, we should have the output be padded by filter size (then cropped)
    s = (I.shape[0] + f.shape[0] - 1), (I.shape[1] + f.shape[1] - 1)
    I_fft = np.fft.fft2(I, s=s)
    f_fft = np.fft.fft2(f, s=s)
    conv = np.real(np.fft.ifft2(I_fft * f_fft))
    return conv[f.shape[0] // 2: -f.shape[0] // 2 + 1, f.shape[1] // 2: -f.shape[1] // 2 + 1]


def find_circles_template_matching(I, min_radius=None, max_radius=None, threshold=None):
    if threshold is None:
        threshold = np.max(I)

    if min_radius is None:
        min_radius = 3

    if max_radius is None:
        max_radius = min(*I.shape[:2])

    # Template matching
    best_conv = []
    best_point = []
    best_radius = []
    for r in range(max_radius, min_radius, -1):
        kernel = create_template(r, False)
        conv = conv2d(I, kernel)
        if np.max(conv) > threshold:
            best_conv += [np.max(conv)]
            best_point += [np.unravel_index(np.argmax(conv, axis=None), I.shape)[::-1]]
            best_radius += [r]

    return best_point, best_radius


def compute_gradient_image(I):
    # First blur
    gaussian = 1 / 16 * np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]])
    blurred_I = conv2d(I, gaussian)

    # Then compute gradient magnitude via sobel filter
    sobel_x = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
    sobel_y = np.flipud(sobel_x.T)
    conv_x = conv2d(blurred_I, sobel_x)
    conv_y = conv2d(blurred_I, sobel_y)
    return conv_x ** 2 + conv_y ** 2


def find_circles_hough_transform(I, min_radius=None, max_radius=None, gradient_threshold=None,
                                 accumulator_threshold=None):
    if gradient_threshold is None:
        gradient_threshold = 100/255 * np.max(I) # opencv uses 100 for uint8 images

    if accumulator_threshold is None:
        accumulator_threshold = 0.99 # We define this as how much of a circle needs to be present to detect circle

    if min_radius is None:
        min_radius = 3

    if max_radius is None:
        max_radius = max(*I.shape[:2])

    # Generate gradient image by smoothening + sobel filter
    edge_image = I

    # Draw a circle at each edge point in image and use as score
    A = np.zeros((max_radius, *I.shape[:2]))
    edges = np.argwhere(edge_image > gradient_threshold)

    # For each radius, draw a circle at all edge points
    angles = np.arange(0, 360) * np.pi / 180
    num_angles = len(angles)
    angles = np.tile(angles, len(edges))
    edges = np.repeat(edges, num_angles, axis=0)
    for radius in range(min_radius, max_radius):
        circle_pts_x = edges[:, 1] + radius * np.cos(angles)
        circle_pts_y = edges[:, 0] + radius * np.sin(angles)

        circle_pts_x = circle_pts_x.astype(int)
        circle_pts_y = circle_pts_y.astype(int)
        mask_x = np.logical_and(circle_pts_x >= 0, circle_pts_x < A.shape[2])
        mask_y = np.logical_and(circle_pts_y >= 0, circle_pts_y < A.shape[1])
        mask = np.logical_and(mask_x, mask_y)

        idx = circle_pts_y[mask] * A.shape[1] + circle_pts_x[mask]
        counts = np.bincount(idx, minlength=A.shape[1] * A.shape[2])
        A[radius, :] += counts.reshape(A.shape[1], A.shape[2])

    max_idx = np.argwhere(A > num_angles * accumulator_threshold)
    return max_idx[:, 1:][:, ::-1], max_idx[:, 0]


def get_warp(dx, dy, scale):
    return scale * np.array([[1, 0, dx], [0, 1, dy]])


def warp(I, W, interpolate=True):
    warped_image = np.zeros_like(I)

    # Get warped points
    x = np.arange(I.shape[1])
    y = np.arange(I.shape[0])
    x, y = np.meshgrid(x, y)
    x = x.reshape(-1, 1)
    y = y.reshape(-1, 1)
    pts = np.hstack([x, y, np.ones_like(x)])
    warped_pts = (W @ pts.T).T

    # Remove points outside of range
    warped_pts[warped_pts[:, 0] < 0] = 0
    warped_pts[warped_pts[:, 1] < 0] = 0
    warped_pts[warped_pts[:, 0] >= I.shape[1]] = 0
    warped_pts[warped_pts[:, 1] >= I.shape[0]] = 0

    # Warp image
    if interpolate:
        # Remove points outside of range for interpolation
        warped_pts[warped_pts[:, 0] >= I.shape[1] - 1] = 0
        warped_pts[warped_pts[:, 1] >= I.shape[0] - 1] = 0

        int_warped_pts = np.floor(warped_pts).astype('int')

        weights = warped_pts - int_warped_pts
        w11 = (1 - weights[:, 0]) * (1 - weights[:, 1])
        w12 = (1 - weights[:, 0]) * weights[:, 1]
        w21 = weights[:, 0] * (1 - weights[:, 1])
        w22 = weights[:, 0] * weights[:, 1]

        top_left = I[int_warped_pts[:, 1], int_warped_pts[:, 0]]
        bottom_left = I[int_warped_pts[:, 1] + 1, int_warped_pts[:, 0]]
        top_right = I[int_warped_pts[:, 1], int_warped_pts[:, 0] + 1]
        bottom_right = I[int_warped_pts[:, 1] + 1, int_warped_pts[:, 0] + 1]

        warped_image[pts[:, 1], pts[:, 0]] = w11 * top_left + w12 * bottom_left + w21 * top_right + w22 * bottom_right
    else:
        warped_pts = warped_pts.astype('int')
        warped_image[pts[:, 1], pts[:, 0]] = I[warped_pts[:, 1], warped_pts[:, 0]]
    return warped_image


def get_patch(I, x_T, r_T):
    return I[x_T[1] - r_T:x_T[1] + r_T + 1, x_T[0] - r_T: x_T[0] + r_T + 1]


def find_warp_klt():
    # TODO: KLT tracker to find circles in image
    # Step 1: find "interest points" by convolving single image of all circles from minradius to maxradius circles, find maxima
    # Step 2: use "interest points" with template of smallest circle and patch of size of biggest circle, find warp for image
    # Step 3: Return circle center = "interest point" + [warp_x, warp_y], radius = maxradius / scale
    pass
