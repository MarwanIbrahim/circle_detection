from implementations import *
import time


def createImage():
    I = np.zeros((480, 640))
    cv2.circle(I, (50, 100), 30, 255, 2)
    cv2.circle(I, (400, 200), 100, 255, 2)
    cv2.rectangle(I, (20, 20), (50, 50), 255, 2)
    return I


def solution1(I):
    '''
    Find circles in the image using template matching
    :param I: Image to detect circles in
    '''
    print("Started solution 1 using template matching")
    tic = time.time()
    max_point, max_radius = find_circles_template_matching(I, 10, 120, 249)
    toc = time.time()
    print(f"Took {toc - tic} seconds")
    print(f"Number of detected circles: {len(max_point)}")
    print(f"Detected circle centers: {max_point}")
    print(f"Detected circle radii: {max_radius}")

    blank_I = np.zeros_like(I)
    for point, radius in zip(max_point, max_radius):
        cv2.circle(blank_I, point, radius, 255, 1)
    cv2.imshow("Found circle", blank_I)
    cv2.imshow("Initial image", I)
    cv2.waitKey(0)


def solution2(I):
    '''
    Find circles in an image using the FFT of an image (ChatGPT recommendation)
    :param I: Image to detect circles in
    '''
    print("Started solution 2 using fourier transform")


def solution3(I):
    '''
    Find circles in an image using KLT to compute an optimal warp from an initial template to the best circle,
    this eliminates the need to loop over radii
    Steps:
        1) create template of filled circle (bagel) from min_radius to max_radius (single kernel)
        2) For each possible circle center -> compute G * I_cropped -> use to update warp
        argmin |I - kernel(warp)| -> optimal warp, error value
        3) Eliminate possible circle if error is above a threshold
        4) return Circle point + (dx, dy),  min_radius * scale
    :param I: Image to detect circles in
    '''
    print("Started solution 3 using KLT to compute optimal warp")


def solution4(I):
    '''
    Find circles in an image by reimplementing the circles hough transform. The main concept is if you draw many circles
    centered on the border of the circle you wish to detect with the same radius as the circle you wish to detect,
    all circles should intersect the most at the circle center point. This accumulation/voting scheme is the main
    principle behind any general hough transform.
    Note: current implementation in find_circles_hough_transform has a bug, can you find it?
    :param I: Image to detect circles in
    '''
    print(
        "Started solution 4 using own implementation of circle hough transform. Note, current implementation "
        "has a bug, expecting wrong result")

    tic = time.time()
    max_point, max_radius = find_circles_hough_transform(I, 10, 120, 200)
    toc = time.time()

    print(f"Took {(toc - tic) * 1e3} milliseconds")
    print(f"Number of detected circles: {len(max_point)}")
    print(f"Detected circle centers: {max_point}")
    print(f"Detected circle radii: {max_radius}")

    blank_I = np.zeros_like(I)
    for point, radius in zip(max_point, max_radius):
        cv2.circle(blank_I, point.astype(int), int(radius), 255, 1)
    cv2.imshow("Found circle", blank_I)
    cv2.imshow("Initial image", I)
    cv2.waitKey(0)


def solution4_cv(I):
    '''
    OpenCV implementation of circle hough transform for validation
    :param I: Image to detect circles in
    '''
    print("Started solution 4 using OpenCV implementation of hough transform. Expect this to be fast")
    I = I.astype(np.uint8)

    tic = time.time()
    circles = cv2.HoughCircles(I, cv2.HOUGH_GRADIENT, 2, 29, minRadius=10, maxRadius=120)[0, ...]
    max_point, max_radius = circles[:, :2], circles[:, -1]
    toc = time.time()

    print(f"Took {(toc - tic) * 1e3} milliseconds")
    print(f"Number of detected circles: {len(max_point)}")
    print(f"Detected circle centers: {max_point}")
    print(f"Detected circle radii: {max_radius}")

    blank_I = np.zeros_like(I)
    for point, radius in zip(max_point, max_radius):
        cv2.circle(blank_I, point.astype(int), int(radius), 255, 1)
    cv2.imshow("Found circle", blank_I)
    cv2.imshow("Initial image", I)
    cv2.waitKey(0)


def main():
    print("Started methods for circle detection, which method will perform the best")
    I = createImage()
    solution1(I)
    solution4(I)
    solution4_cv(I)


if __name__ == "__main__":
    main()
